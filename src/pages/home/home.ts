import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { StorageProvider } from '../../providers/storage/storage';
import { PushserviceProvider } from '../../providers/pushservice/pushservice';


declare var WL;
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  adapterResult:any="";
  AuthHandler: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage:StorageProvider,
    public push:PushserviceProvider,
    public service:ServicesProvider,
    public alert:AlertController) {
  }
  ionViewCanEnter() {
    console.log('ionViewDidLoad HomePage');
    this.storage.jsonstoreInitialize();
    this.mfpAuthInit();
  }
  /**
   * Method for invoking adapter call from services
   */
  invokeAdapter(){
    this.push.triggerPushNotification();
    this.service.showLoading("Loading...")
    this.service.invokeAdapterCall("ramdomuser","getRandomUser","get","").then((response:any)=>{
      console.log("response from adapter call is",response);
      this.adapterResult = response.results;
      if(this.adapterResult){
        this.storage.jsonstoreAdd("userData",this.adapterResult).then((response:any)=>{
          if(response){
            console.log("data added sucessfully");
          }
        },(error)=>{
          console.log("data added  from jsonstore error",error);
        });
      }
      this.service.dismissLoading();
    });
  }
  /**
   * Method for reading  json data from local jsonstore
   */
  readJsonData(){
    this.storage.jsonstoreReadAll("userData").then((jsonData:any)=>{
      if(jsonData){
        console.log("Data readed from jsonstore",jsonData);
      };
    },(error)=>{
      console.log("Data readed from jsonstore error",error);
    });
  }
  /**
   * Method to handle the userlogin security check
   * It is scope restricted
   */
  mfpAuthInit(){
    this.AuthHandler = WL.Client.createSecurityCheckChallengeHandler("UserLogin");
    this.AuthHandler.handleChallenge = ((response)=>{
      console.log("I am in challenge handler method");
      if(response.errorMsg){
        var msg = response.errorMsg+"<br>";
        msg+="Remaining Attempts : "+response.remainingAttempts;
      }
      this.displayChallenge(msg);
    });
  }
  /**
   * Method to handle the user input and password
   * this retireves input data from  user and verifies with input logic in userlogin adapter
   */
  displayChallenge(msg){
    let prompt = this.alert.create({
      title: 'Login',
      message: msg,
      inputs: [
        {
          name: 'username',
          placeholder: 'Username'
        },
        {
          name: 'password',
          placeholder: 'Password',
          type:'password'
        },
      ],
      buttons: [
        {
          text: 'Login',
          handler: data => {
            console.log("data submited is",data);
            this.AuthHandler.submitChallengeAnswer(data);
          }
        }
      ]
    });
    prompt.present();
  }
}
