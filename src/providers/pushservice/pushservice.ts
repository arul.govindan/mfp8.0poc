import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

declare var MFPPush;
@Injectable()
export class PushserviceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello PushserviceProvider Provider');
  }
  /**
   * Method for triggering push notification
   */
  triggerPushNotification(){
    var options = {};
    var tag =['messenger'];
    MFPPush.initialize((onSuccess)=>{
      console.log("-->Push initialized Success",onSuccess);
      MFPPush.registerNotificationsCallback((recievedMessage)=>{
        console.log("Recieved push notification is",recievedMessage);
        alert(recievedMessage.alert);
      });
      MFPPush.registerDevice(options,(onRegSuccess)=>{
        console.log("-->Push device register Success",onRegSuccess);
        MFPPush.subscribe(tag,(onSubSuccess)=>{
          console.log("-->Push device Subscribed Success",onSubSuccess);
        },(onSubFailure)=>{
          console.log("-->Push device Subscribed Failure",onSubFailure);
        });
      },(onRegFailure)=>{
        console.log("-->Push device register failure",onRegFailure);
      });
    },(onFailure)=>{
      console.log("-->Push initialized failure",onFailure);
    });
  }

}
